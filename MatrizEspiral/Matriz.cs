﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MatrizEspiral
{
    class Matriz
    {
        private int[,] espiral;
        private int qtdLinhas;
        private int qtdColunas;

        public Matriz (int linhas, int colunas)
        {
            qtdLinhas = linhas;
            qtdColunas = colunas;
            espiral = new int[linhas, colunas];
        }

        public void preencheMatriz()
        {
            int qtdPosicoes = qtdLinhas * qtdColunas;

            int direitaInicio = 0;
            int direitaFinal = qtdColunas - 1;
            int baixoInicio = 1;
            int baixoFinal = qtdLinhas - 1;
            int esquerdaInicio = qtdColunas -2;
            int esquerdaFinal = 0;
            int cimaInicio = qtdLinhas - 2;
            int cimaFinal = 1;

            int fixoDireita = 0;
            int fixoBaixo = qtdColunas - 1;
            int fixoEsquerda = qtdLinhas - 1;
            int fixoCima = 0;

            int numeroDaVolta = 1;
            int valorAtual = 1;

            while (valorAtual <= qtdPosicoes)
            {
                switch (numeroDaVolta % 4)
                {
                    case 1:
                        for (int i = direitaInicio; i <= direitaFinal; i++)
                        {
                            espiral[fixoDireita, i] = valorAtual;
                            valorAtual++;
                        }
                        direitaInicio++;
                        direitaFinal--;
                        fixoDireita++;
                        break;
                    case 2:
                        for (int i = baixoInicio; i <= baixoFinal; i++)
                        {
                            espiral[i, fixoBaixo] = valorAtual;
                            valorAtual++;
                        }
                        baixoInicio++;
                        baixoFinal--;
                        fixoBaixo--;
                        break;
                    case 3:
                        for (int i = esquerdaInicio; i >= esquerdaFinal; i--)
                        {
                            espiral[fixoEsquerda, i] = valorAtual;
                            valorAtual++;
                        }
                        esquerdaInicio--;
                        esquerdaFinal++;
                        fixoEsquerda--;
                        break;
                    case 0:
                        for (int i = cimaInicio; i >= cimaFinal; i--)
                        {
                            espiral[i, fixoCima] = valorAtual;
                            valorAtual++;
                        }
                        cimaInicio--;
                        cimaFinal++;
                        fixoCima++;
                        break;
                }
                numeroDaVolta++;
            }

        }

        public void imprimeMatriz()
        {
            for(int linha =0; linha < espiral.GetLength(0); linha++)
            {
                for (int coluna = 0; coluna < espiral.GetLength(1); coluna++)
                {
                    Console.Write(espiral[linha, coluna] + "  ");

                }
                Console.WriteLine();
            }
        }
    }
}
