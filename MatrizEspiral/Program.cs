﻿using System;
using System.Diagnostics;

namespace MatrizEspiral
{
    class Program
    {
        static void Main(string[] args)
        {

            Matriz matriz = new Matriz(20, 23);

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            matriz.preencheMatriz();

            stopwatch.Stop();
            Console.WriteLine($"Tempo de processamento: {stopwatch.Elapsed}");


            matriz.imprimeMatriz();

            Console.WriteLine();

            Console.ReadKey();
        }
    }
}
